import { Component } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			header: "",
			closeButton: "",
			text: "",
			actions: "",
			modalClosed: true,
		};
	}

	showFirstModal() {
		this.setState({
			modalClosed: false,
			header: "Do you want to delete this file?",
			closeButton: true,
			text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
			actions: (
				<div className="modal__actions">
					<button
						className="modal__actions-button"
						onClick={() => this.handleClose()}>
						Ok
					</button>
					<button
						className="modal__actions-button"
						onClick={() => this.handleClose()}>
						Close
					</button>
				</div>
			),
		});
	}

	showSecondModal() {
		this.setState({
			modalClosed: false,
			header: "Do you want to create this file?",
			closeButton: true,
			text: "Are you sure you want to create it?",
			actions: (
				<div className="modal__actions">
					<button
						className="modal__actions-button"
						onClick={() => this.handleClose()}>
						Yes
					</button>
					<button
						className="modal__actions-button"
						onClick={() => this.handleClose()}>
						No
					</button>
				</div>
			),
		});
	}

	handleClose = () => {
		this.setState({ modalClosed: true });
	};

	render() {
		return (
			<div className="App">
				<Button
					backgroundColor={"red"}
					text={"Open first modal"}
					onClick={() => {
						this.showFirstModal();
					}}></Button>
				<Button
					backgroundColor={"green"}
					text={"Open second modal"}
					onClick={() => {
						this.showSecondModal();
					}}></Button>
				<Modal
					header={this.state.header}
					closeButton={this.state.closeButton}
					text={this.state.text}
					actions={this.state.actions}
					modalClosed={this.state.modalClosed}
					close={this.handleClose}></Modal>
			</div>
		);
	}
}

export default App;
