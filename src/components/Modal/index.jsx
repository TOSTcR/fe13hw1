import { Component } from "react";
import "./style.scss";

class Modal extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div
				className={this.props.modalClosed ? null : "overlay"}
				onClick={(e) => {
					if (e.target.className == "overlay") {
						this.props.close();
					}
				}}>
				<div
					className="modal "
					style={
						this.props.modalClosed ? { display: "none" } : { display: "flex" }
					}>
					<div className="modal__upperModal">
						<h3 className="modal__title">{this.props.header}</h3>
						{this.props.closeButton === true ? (
							<span
								className="modal__closeButton"
								onClick={() => {
									this.props.close();
								}}>
								X
							</span>
						) : null}
					</div>
					<p className="modal__text">{this.props.text}</p>
					{this.props.actions}
				</div>
			</div>
		);
	}
}

export default Modal;
