import { Component } from "react";
import "./style.scss";

class Button extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div
				className="button"
				onClick={this.props.onClick}
				style={{ backgroundColor: this.props.backgroundColor }}>
				{this.props.text}
			</div>
		);
	}
}

export default Button;
